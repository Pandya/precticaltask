//
//  Task.swift
//  PrecticalTask
//
//  Created by ZB_Mac_Mini_Nirav on 17/12/21.
//

import UIKit

class Task: NSObject {

    var title : String?
    var isAlert : Bool?
    var taskDate : Date?
    var isComplete : Bool = false
    
    override init() {
        super.init()
    }
    
    init(title:String, isAlert:Bool, taskDate:Date, isComplete:Bool) {
        
        self.title = title
        self.isAlert = isAlert
        self.taskDate = taskDate
        self.isComplete = isComplete
    }
}
