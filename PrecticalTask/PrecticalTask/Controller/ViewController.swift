//
//  ViewController.swift
//  PrecticalTask
//
//  Created by ZB_Mac_Mini_Nirav on 17/12/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var clvTaskList: UICollectionView!
    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var progressView: UISlider!
    
    var arrTask = [Task]()
    var selectedIndex : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblName.text = UIDevice.current.name
        self.updateProgress()
    }
    
    func updateProgress() {
        
        if  self.arrTask.count > 0 {
            let CompleteTask = self.arrTask.filter { $0.isComplete }
            let progrss = Float(CompleteTask.count) / Float(self.arrTask.count)
            self.progressView.value = Float(progrss)
            self.lblProgress.text = "\(progrss * 100)%"
        } else {
            self.progressView.value = 0
            self.lblProgress.text = "0%"
        }
    }
    
    
    @IBAction func btnAddClick(_ sender: Any) {
        
        let vc = storyBoards.Main.instantiateViewController(withIdentifier: "AddTaskVC") as! AddTaskVC
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        vc.updateList = { (value) in
            self.arrTask.append(value)
            self.clvTaskList.reloadData()
            self.updateProgress()
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btnCheckClick(_ sender: UIButton) {
        
        if self.arrTask[sender.tag].isComplete {
            self.arrTask[sender.tag].isComplete = false
        } else {
            self.arrTask[sender.tag].isComplete = true
        }
        self.clvTaskList.reloadData()
        self.updateProgress()
    }
}

extension ViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrTask.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : TaskCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaskCell", for: indexPath) as! TaskCell
        let item = self.arrTask[indexPath.row]
        cell.lblDescription.text = item.title
        cell.imgCheck.image = item.isComplete ? #imageLiteral(resourceName: "ic_checkmark") : nil
        cell.lblDay.text = item.taskDate?.day
        cell.lblMonth.text = item.taskDate?.monthname
        cell.lblName.text = item.taskDate?.dayname
        cell.btnCheck.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ScreenSize.WIDTH / 2 - 15, height: ScreenSize.WIDTH / 2 - 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        configureContextMenu(index: indexPath.row)
    }
    
    func configureContextMenu(index: Int) -> UIContextMenuConfiguration{
        let context = UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { (action) -> UIMenu? in
            
            let edit = UIAction(title: "Edit", image: UIImage(systemName: "square.and.pencil"), identifier: nil, discoverabilityTitle: nil, state: .off) { (_) in
                print("edit button clicked")
                
                let vc = storyBoards.Main.instantiateViewController(withIdentifier: "AddTaskVC") as! AddTaskVC
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                vc.isEditTask = true
                vc.task = self.arrTask[index]
                vc.updateList = { (value) in
                    self.arrTask[index] = value
                    self.clvTaskList.reloadData()
                    self.updateProgress()
                }
                self.present(vc, animated: true, completion: nil)
            }
            let delete = UIAction(title: "Delete", image: UIImage(systemName: "trash"), identifier: nil, discoverabilityTitle: nil,attributes: .destructive, state: .off) { (_) in
                print("delete button clicked")
                self.arrTask.remove(at: index)
                self.clvTaskList.reloadData()
                self.updateProgress()
            }
            return UIMenu(title: "Options", image: nil, identifier: nil, options: UIMenu.Options.displayInline, children: [edit,delete])
        }
        return context
    }
}


class TaskCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnCheck: UIControl!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}
