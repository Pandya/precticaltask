//
//  AddTaskVC.swift
//  PrecticalTask
//
//  Created by ZB_Mac_Mini_Nirav on 17/12/21.
//

import UIKit
import DatePickerDialog

class AddTaskVC: UIViewController {

    @IBOutlet weak var txtTitle: PaddingTextField!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var swtAlert: UISwitch!
    
    var updateList : ((_ value:Task) -> (Void))?
    var task = Task()
    var isEditTask : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isEditTask {
            self.txtTitle.text = self.task.title
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM,yyyy"
            self.lblDate.text = dateFormatter.string(from: self.task.taskDate!)
            dateFormatter.dateFormat = "hh:mm a"
            self.lblTime.text = dateFormatter.string(from: self.task.taskDate!)
        }
    }
    
    @IBAction func btnSelectDateClick(_ sender: Any) {
    
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), datePickerMode: .date) { date in
            
            if let dt = date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM,yyyy"
                self.lblDate.text = dateFormatter.string(from: dt)
            }
        }
    }
    
    @IBAction func btnSelectTimeClick(_ sender: Any) {
    
        DatePickerDialog().show("Select Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), datePickerMode: .time) { date in
            
            if let dt = date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm a"
                self.lblTime.text = dateFormatter.string(from: dt)
            }
        }
    }
    
    @IBAction func btnCloseClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSaveClick(_ sender: Any) {
        
        let strDate = self.lblDate.text! + " " + self.lblTime.text!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM,yyyy hh:mm a"
        let date = dateFormatter.date(from: strDate)
        let objTask = Task(title: self.txtTitle.text!, isAlert: self.swtAlert.isOn, taskDate: date!, isComplete: false)
        if self.swtAlert.isOn {
            appDelegate.scheduleNotification(notificationMessage: self.txtTitle.text!, identifier: randomString(length: 6), date: objTask.taskDate!)
        }
        self.updateList?(objTask)
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
